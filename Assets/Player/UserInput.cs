﻿using UnityEngine;
using System.Collections;

using GameLogic;

public class UserInput : MonoBehaviour {

	private Player player;

	// Use this for initialization
	void Start () {
		//get the game object this script belongs to and get the component of type player (script)
		player = transform.root.GetComponent<Player> ();

	}
	
	// Update is called once per frame
	void Update () {
		Camera.SetupCurrent (Camera.allCameras [0]);
		if (player.isHuman) {
			MoveCamera();
			RotateCamera();
			MouseActivity();
		}
	}

	private void MouseActivity() {
		if(Input.GetMouseButtonDown(0)) LeftMouseClick();
		else if(Input.GetMouseButtonDown(1)) RightMouseClick();
	}

	private void LeftMouseClick(){
		if(player.hud.MouseInBounds()) {
			GameObject hitObject = FindHitObject();
			Vector3 hitPoint = FindHitPoint();
			if(hitObject && hitPoint != ResourceManager.InvalidPosition) {
				if(player.SelectedObject) player.SelectedObject.MouseClick(hitObject, hitPoint, player);
				else if(hitObject.name!="Ground") {
					WorldObject worldObject = hitObject.transform.root.GetComponent< WorldObject >();
					if(worldObject) {
						//we already know the player has no selected object
						player.SelectedObject = worldObject;
						worldObject.SetSelection(true, player.hud.GetPlayingArea());
					}
				}
			}
		}
	}
	
	private void RightMouseClick(){
		if(player.hud.MouseInBounds() && !Input.GetKey(KeyCode.LeftAlt) && player.SelectedObject) {
			player.SelectedObject.SetSelection(false, player.hud.GetPlayingArea());
			player.SelectedObject = null;
		}
	}
	
	private GameObject FindHitObject() {
		Ray ray = Camera.allCameras[0].ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit)) return hit.collider.gameObject;
		return null;
	}

	private Vector3 FindHitPoint() {
		Ray ray = Camera.allCameras[0].ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit)) return hit.point;
		return ResourceManager.InvalidPosition;
	}
	
	private void MoveCamera(){
		bool mouseScroll = false;

		float xPos = Input.mousePosition.x;
		float yPos = Input.mousePosition.y;
		Vector3 movement = new Vector3 (0, 0, 0);

		int scrollWidth = ResourceManager.ScrollWidth;
		if (xPos >= 0 && xPos < scrollWidth) {
			movement.x -= ResourceManager.ScrollSpeed;
			mouseScroll = true;
			player.hud.SetCursorState(CursorState.PanLeft);
		} else if (xPos <= Screen.width && xPos > Screen.width - scrollWidth) {
			movement.x += ResourceManager.ScrollSpeed;
			mouseScroll = true;
			player.hud.SetCursorState(CursorState.PanRight);
		}

		if (yPos >= 0 && yPos < scrollWidth) {
			mouseScroll = true;
			movement.z -= ResourceManager.ScrollSpeed;
			player.hud.SetCursorState(CursorState.PanDown);
		} else if (yPos <= Screen.height && yPos > Screen.height - scrollWidth) {
			mouseScroll = true;
			movement.z += ResourceManager.ScrollSpeed;
			player.hud.SetCursorState(CursorState.PanUp);
		}


		//move in the directio camera is pointing ignoring y

		movement = Camera.current.transform.TransformDirection (movement);
		movement.y = 0;

		//away from ground movement ??????
		movement.y -= ResourceManager.ScrollSpeed * Input.GetAxis ("Mouse ScrollWheel");


		Vector3 origin = Camera.current.transform.position;
		Vector3 destination = origin;
		destination.x += movement.x;
		destination.y += movement.y;
		destination.z += movement.z;

		if (destination.y > ResourceManager.MaxCameraHeight)
			destination.y = ResourceManager.MaxCameraHeight;

		if (destination.y < ResourceManager.MinCameraHeight)
			destination.y = ResourceManager.MinCameraHeight;

		if (destination != origin) {
			Camera.current.transform.position = Vector3.MoveTowards(origin, destination, Time.deltaTime * ResourceManager.ScrollSpeed);
		}

		if(!mouseScroll) {
			player.hud.SetCursorState(CursorState.Select);
		}

	}

	private void RotateCamera(){
		Vector3 origin = Camera.current.transform.eulerAngles;
		Vector3 destination = origin;

		if ((Input.GetKey (KeyCode.LeftAlt) || Input.GetKey (KeyCode.RightAlt)) && Input.GetMouseButton (1)) {
			destination.x -= Input.GetAxis("Mouse Y") * ResourceManager.RotateAmount;
			destination.y += Input.GetAxis("Mouse X") * ResourceManager.RotateAmount;
		}

		if (destination != origin) {
			Camera.current.transform.eulerAngles = Vector3.MoveTowards(origin, destination, Time.deltaTime * ResourceManager.RotateSpeed);
		}
	}
}
