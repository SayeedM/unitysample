﻿using UnityEngine;
using System.Collections;

namespace GameLogic{
	public static class ResourceManager{
		public static float ScrollSpeed { get { return 25; } }
		public static float RotateSpeed { get { return 100; } }

		/* margin from edge of the screen - the area where mouse move will cause scroll */
		public static int ScrollWidth { get { return 15; } }

		public static int MaxCameraHeight { get { return 40; } }
		public static int MinCameraHeight { get { return 10; } }

		public static int RotateAmount { get { return 10; } }


		private static Vector3 invalidPosition = new Vector3(-99999, -99999, -99999);
		public static Vector3 InvalidPosition { get { return invalidPosition; } }

		private static GUISkin selectBoxSkin;
		public static GUISkin SelectBoxSkin { get { return selectBoxSkin; } }
		
		public static void StoreSelectBoxItems(GUISkin skin) {
			selectBoxSkin = skin;
		}

		private static Bounds invalidBounds = new Bounds(new Vector3(-99999, -99999, -99999), new Vector3(0, 0, 0));
		public static Bounds InvalidBounds { get { return invalidBounds; } }


	}
}