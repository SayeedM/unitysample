﻿using UnityEngine;
using System.Collections;

namespace GameLogic{
	public enum CursorState {
		Select, Move, Attack, PanLeft, PanRight, PanUp, PanDown, Harvest
	}
}
